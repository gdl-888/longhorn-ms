---
title: Drivers for VMWare
author: Thomas Hounsell
type: post
date: 2020-09-01T00:00:00+00:00
url: /vmware-drivers/
categories:
  - Downloads

---

These packages are some extracted drivers from the VMWare Guest Additions. The tools themselves are not compatible with Longhorn, although the drivers themselves maintained compatibility for much longer.

* [VMware 6.5 driver package](/download/vmware-6-5-driver-package.zip)
* [VMware 6.5.2 driver package](/download/vmware-6-5-2-driver-package.zip)
* [VMware 7.0 driver package](/download/vmware-7-0-driver-package.zip)