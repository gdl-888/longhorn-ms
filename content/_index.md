---
title: "Experience Longhorn"
---

# "Long what?"

"Longhorn" was the codename for the operating system that was to be Windows XP's successor. It was initially intended to be a minor release - reflected by its codename, taken from the name of a bar situated between the mountains of Whistler (the codename of Windows XP) and Blackcomb (which was planned as the next major release of Windows). As Longhorn began development, it grew in scope until it became the next major release of Windows instead, and Blackcomb was shelved.

With its expanded scope and ambitious targets, the project suffered endless technical problems that eventually got so severe, Microsoft reset the project and started again from scratch. Over three years after the original planned launch, Microsoft released "Longhorn" as Windows Vista in January 2007.

The goal of the Experience Longhorn project is to comprehensively document the Longhorn project.

Longhorn.ms is currently maintained by Thomas Hounsell. You can submit changes via pull requests on [our GitLab repository](https://gitlab.com/experience-longhorn/longhorn-ms).
